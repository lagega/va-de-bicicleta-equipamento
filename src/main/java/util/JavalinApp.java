package util;

import static io.javalin.apibuilder.ApiBuilder.*;
import io.javalin.Javalin;

import servicos.ServicosBicicleta;
import servicos.ServicosTotem;
import servicos.ServicosTranca;

public class JavalinApp {
    final static String ID_BICICLETA_PATH = "/bicicleta/:idBicicleta";
    final static String ID_TRANCA_PATH = "/tranca/:idTranca";
    final Javalin app = Javalin.create(config -> config.defaultContentType = "application/json").routes(() -> {
                path("/bicicleta",  ()-> get(ServicosBicicleta::recuperarBicicletas));

                path("/bicicleta",  ()-> post(ServicosBicicleta::cadastrarBicicleta));

                path("/bicicleta/integrarNaRede",  () -> post(ServicosBicicleta::integrarNaRede));

                path("/bicicleta/retirarDaRede", ()-> post(ServicosBicicleta::retirarDaRede));

                path(ID_BICICLETA_PATH, ()-> get(ServicosBicicleta::obterBicicleta));

                path(ID_BICICLETA_PATH, ()-> put(ServicosBicicleta::editarBicicleta));

                path(ID_BICICLETA_PATH, ()-> delete(ServicosBicicleta::removerBicicleta));

                path(ID_BICICLETA_PATH + "/status/:acao", ()-> post(ServicosBicicleta::alterarStatus));

                path("/totem",  ()-> get(ServicosTotem::recuperarTotens));

                path("/totem",  ()-> post(ServicosTotem::cadastrarTotem));

                path("/tranca/integrarNaRede",  () -> post(ServicosTranca::integrarNaRede));

                path("/tranca/retirarDaRede", ()-> post(ServicosTranca::retirarDaRede));

                path("/totem/:idTotem", ()-> put(ServicosTotem::editarTotem));

                path("/totem/:idTotem", ()-> delete(ServicosTotem::removerTotem));

                path("/totem/:idTotem/trancas", ()-> get(ServicosTotem::listarTrancas));

                path("/totem/:idTotem/bicicletas", ()-> get(ServicosTotem::listarBicicletas));

                path("/tranca",  ()-> get(ServicosTranca::recuperarTrancas));

                path("/tranca",  ()-> post(ServicosTranca::cadastrarTranca));

                path(ID_TRANCA_PATH,  ()-> get(ServicosTranca::obterTranca));

                path(ID_TRANCA_PATH,  ()-> put(ServicosTranca::editarTranca));

                path(ID_TRANCA_PATH,  ()-> delete(ServicosTranca::removerTranca));

                path("/tranca/:idTranca/bicicleta",  ()-> get(ServicosTranca::bicicletaNaTranca));

                path("/tranca/:idTranca/status/:acao",  ()-> post(ServicosTranca::alterarStatus));

            }
    );

    public void start(int port)
    {
        this.app.start(port);
    }

    public void stop()
    {
        this.app.stop();
    }
}