package models;

public class TotemTranca {
	public String idTotem;
	public String idTranca;
	
	public TotemTranca(String idTotem, String idTranca) {
		this.idTotem = idTotem;
		this.idTranca = idTranca;
	}
}
