package models;

import java.util.UUID;

public class Erro {
	public String codigo;
	public String id;
	public String mensagem;
	
	public Erro(String codigo, String mensagem) {
		this.codigo = codigo;
		this.mensagem = mensagem;

		this.id = UUID.randomUUID().toString();
	}
}
