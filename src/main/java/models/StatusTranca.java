package models;

public enum StatusTranca {
	LIVRE, OCUPADA, NOVA, APOSENTADA, EM_REPARO
}
