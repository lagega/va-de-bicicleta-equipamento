package models;

public class TrancaBicicleta {
	public String idBicicleta;
	public String idTranca;
	
	public TrancaBicicleta(String idTranca, String idBicicleta) {
		this.idBicicleta = idBicicleta;
		this.idTranca = idTranca;
	}
}
