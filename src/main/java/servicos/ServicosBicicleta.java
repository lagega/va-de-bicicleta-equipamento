package servicos;

import equipamento.Bicicleta;
import equipamento.Tranca;
import io.javalin.http.Context;
import models.Erro;
import models.StatusBicicleta;
import models.StatusTranca;
import models.TrancaBicicleta;

import java.time.Year;
import java.util.ArrayList;
import java.util.LinkedHashMap;

import static servicos.ServicosEmail.enviaEmail;

public class ServicosBicicleta {
	static final ArrayList<Bicicleta> bicicletas = new ArrayList<>();
	static final String DADOS_INVALIDOS = "Dados Inválidos";
	static final String ID_BICICLETA = "idBicicleta";

	private ServicosBicicleta() {
		throw new IllegalStateException("Classe de Servicos");
	}
	public static void recuperarBicicletas(Context ctx) {
		if (bicicletas.isEmpty()) {
			ctx.html("Não há bicicletas cadastradas");
		} else {
			ctx.json(bicicletas);
		}
		ctx.status(200);
	}
	
	public static void cadastrarBicicleta(Context ctx) {
		try {
			LinkedHashMap<String, String> request = ctx.bodyAsClass(LinkedHashMap.class);
			Bicicleta bicicleta = criaBicicleta(request);

			adicionaBicicleta(bicicleta);

			ctx.json(bicicleta);
			ctx.status(200);
		} catch (IllegalArgumentException e) {
			Erro erro = new Erro("422", DADOS_INVALIDOS);
			ctx.json(erro);
	        ctx.status(422);
		}
	}
	
	public static void integrarNaRede(Context ctx) {
		try {
			LinkedHashMap<String, ?> request = ctx.bodyAsClass(LinkedHashMap.class);
			TrancaBicicleta trancaBicicleta = criaTrancaBicicleta(request);
			
			Bicicleta bicicleta = buscaBicicleta(trancaBicicleta.idBicicleta);
			if (!bicicleta.getStatus().equals("NOVA") && !bicicleta.getStatus().equals("EM_REPARO")) {
				ctx.status(422);
				throw new IllegalArgumentException("Status da Bicicleta inválido");
			}
			
			Tranca tranca = ServicosTranca.buscaTranca(trancaBicicleta.idTranca);
			if (!tranca.getStatus().equals("LIVRE")) {
				ctx.status(422);
				throw new IllegalArgumentException("Status da Tranca inválido");
			}

			bicicleta.setStatus(StatusBicicleta.DISPONIVEL);
			tranca.setStatus(StatusTranca.OCUPADA);
			tranca.setBicicleta(bicicleta.getId());
			enviaEmail("bicicleta", bicicleta.getId(), "inserida");
			ctx.status(200);
			
		} catch (IllegalArgumentException e) {
			Erro erro = new Erro("422", DADOS_INVALIDOS);
			ctx.json(erro);
	        ctx.status(422);
		}
	}
	
	public static void retirarDaRede(Context ctx) {
		try {
			LinkedHashMap<String, ?> request = ctx.bodyAsClass(LinkedHashMap.class);
			TrancaBicicleta trancaBicicleta = criaTrancaBicicleta(request);
			
			Tranca tranca = ServicosTranca.buscaTranca(trancaBicicleta.idTranca);
			if(!tranca.getBicicleta().equals(trancaBicicleta.idBicicleta)) {
				throw new IllegalArgumentException("Id da Tranca ou da Bicicleta inválido para esta operação");
			}
			
			Bicicleta bicicleta = buscaBicicleta(trancaBicicleta.idBicicleta);			
			if (bicicleta.getStatus().equals("REPARO_SOLICITADO") && tranca.getStatus().equals("OCUPADA")) {
				
				bicicleta.setStatus(StatusBicicleta.EM_REPARO);
				tranca.setStatus(StatusTranca.LIVRE);

				enviaEmail("bicicleta", bicicleta.getId(), "removida");
				ctx.status(200);
			} else throw new IllegalArgumentException("Status da Tranca ou da Bicicleta inválido para esta operação");
		} catch (IllegalArgumentException e) {
			Erro erro = new Erro("422", DADOS_INVALIDOS);
			ctx.json(erro);
	        ctx.status(422);
		}
	}
	
	public static void obterBicicleta(Context ctx) {
		try {
			String id = ctx.pathParam(ID_BICICLETA);
			Bicicleta bicicleta = buscaBicicleta(id);
			
			ctx.json(bicicleta);
			ctx.status(200);
		} catch (NullPointerException e) {
			Erro erro = new Erro("404", e.getMessage());
			ctx.json(erro);
	        ctx.status(404);
		}
	}
	
	public static void editarBicicleta(Context ctx) {
		try {
			String idBicicleta = ctx.pathParam(ID_BICICLETA);

			LinkedHashMap<String, ?> request = ctx.bodyAsClass(LinkedHashMap.class);
			Bicicleta bicicletaNova = criaBicicleta(request);
			Bicicleta bicicletaAntiga = buscaBicicleta(idBicicleta);
			
			bicicletaAntiga.setMarca(bicicletaNova.getMarca());
			bicicletaAntiga.setModelo(bicicletaNova.getModelo());
			bicicletaAntiga.setAno(bicicletaNova.getAno());
			bicicletaAntiga.setNumero(bicicletaNova.getNumero());
			bicicletaAntiga.setStatus(StatusBicicleta.valueOf(bicicletaNova.getStatus()));
			
			ctx.json(bicicletaAntiga);
			ctx.status(200);
		} catch (IllegalArgumentException e) {
			Erro erro = new Erro("422", DADOS_INVALIDOS);
			ctx.json(erro);
	        ctx.status(422);
		}
	}
	
	public static void removerBicicleta(Context ctx) {
		try {
			String idBicicleta = ctx.pathParam(ID_BICICLETA);
			int idAchado = 0;
			
			for (int i = 0; i < bicicletas.size(); i++) {
				if (bicicletas.get(i).getId().equals(idBicicleta)) {
					bicicletas.remove(i);
		        	ctx.status(200);
					idAchado = 1;
		        	break;
				}
			}
			if (idAchado == 0) {
				throw new NullPointerException("idBicicleta não encontrado");
			}
		} catch (NullPointerException e) {
			Erro erro = new Erro("404", e.getMessage());
			ctx.json(erro);
	        ctx.status(404);
		}
	}
	
	public static void alterarStatus(Context ctx) {
		try {
			String idBicicleta = ctx.pathParam(ID_BICICLETA);
			String acao = ctx.pathParam("acao");
			
			Bicicleta bicicleta = buscaBicicleta(idBicicleta);
			bicicleta.setStatus(StatusBicicleta.valueOf(acao));
			
			ctx.json(bicicleta);
			ctx.status(200);
		} catch (IllegalArgumentException e) {
			Erro erro = new Erro("422", DADOS_INVALIDOS);
			ctx.json(erro);
	        ctx.status(422);
		}
	}

	public static Bicicleta buscaBicicleta(String idBicicleta) {
		for (Bicicleta bicicleta : bicicletas) {
			if (bicicleta.getId().equals(idBicicleta)) {
				return bicicleta;
			}
		}
		throw new NullPointerException("idBicicleta não encontrado");
	}

	public static Bicicleta criaBicicleta(LinkedHashMap<String, ?> request) {
		String marca = request.get("marca").toString();
		String modelo = request.get("modelo").toString();
		Year ano = Year.of(Integer.parseInt(request.get("ano").toString()));
		int numero = Integer.parseInt(request.get("numero").toString());
		StatusBicicleta status = StatusBicicleta.valueOf(request.get("status").toString());

		return new Bicicleta(marca, modelo, ano, numero, status);
	}

	public static TrancaBicicleta criaTrancaBicicleta(LinkedHashMap<String, ?> request) {
		String idBicicleta = request.get(ID_BICICLETA).toString();
		String idTranca = request.get("idTranca").toString();

		return new TrancaBicicleta(idTranca, idBicicleta);
	}

	public static void adicionaBicicleta(Bicicleta bicicleta) {
		bicicletas.add(bicicleta);
	}
}

