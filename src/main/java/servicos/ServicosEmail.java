package servicos;

import kong.unirest.Unirest;

public class ServicosEmail {
    private ServicosEmail() {
        throw new IllegalStateException("Classe de Servicos");
    }

    public static void enviaEmail(String tipo, String id, String metodo) {
        String body = String.format(
                "{" +
                        "  \"email\": \"funcionario@vadebicicleta.com\",\n" +
                        "  \"mensagem\": \"A %s %s foi %s com sucesso\"\n" +
                        "}",
                tipo,
                id,
                metodo);
        String url = "https://teamkrypto-vadebicicleta.herokuapp.com/enviarEmail";

        Unirest.post(url).body(body).asString();
    }
}
