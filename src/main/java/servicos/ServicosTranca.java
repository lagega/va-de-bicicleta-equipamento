package servicos;

import io.javalin.http.Context;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import equipamento.Bicicleta;
import equipamento.Tranca;
import models.Erro;
import models.StatusTranca;
import models.TotemTranca;

import static servicos.ServicosEmail.enviaEmail;

public class ServicosTranca {
	static final ArrayList<Tranca> trancas = new ArrayList<>();
	static final String DADOS_INVALIDOS = "Dados Inválidos";
	static final String ID_TRANCA = "idTranca";

	private ServicosTranca() {
		throw new IllegalStateException("Classe de Servicos");
	}
	
	public static void integrarNaRede(Context ctx) {
		try {
			LinkedHashMap<String, ?> request = ctx.bodyAsClass(LinkedHashMap.class);
			TotemTranca totemTranca = criaTotemTranca(request);
			
			Tranca tranca = buscaTranca(totemTranca.idTranca);
			if (!tranca.getStatus().equals("NOVA") && !tranca.getStatus().equals("EM_REPARO")) {
				throw new IllegalArgumentException("Status da Tranca inválido");
			}
			
			ServicosTotem.buscaTotem(totemTranca.idTotem);
			tranca.setStatus(StatusTranca.LIVRE);
			enviaEmail("tranca", tranca.getId(), "inserida");
			ctx.status(200);
		} catch (IllegalArgumentException e) {
			Erro erro = new Erro("422", DADOS_INVALIDOS);
			ctx.json(erro);
	        ctx.status(422);
		}
	}
	
	public static void retirarDaRede(Context ctx) {
		try {
			LinkedHashMap<String, ?> request = ctx.bodyAsClass(LinkedHashMap.class);
			TotemTranca totemTranca = criaTotemTranca(request);
			
			Tranca tranca = buscaTranca(totemTranca.idTranca);
			if (!tranca.getStatus().equals("LIVRE")) {
				throw new IllegalArgumentException("Status da Tranca inválido");
			}
			
			tranca.setStatus(StatusTranca.EM_REPARO);
			enviaEmail("tranca", tranca.getId(), "removida");
			ctx.status(200);
		} catch (IllegalArgumentException e) {
			Erro erro = new Erro("422", DADOS_INVALIDOS);
			ctx.json(erro);
	        ctx.status(422);
		}
	}
	
	public static void recuperarTrancas(Context ctx) {
		ctx.json(trancas);
		ctx.status(200);
	}
	
	public static void cadastrarTranca(Context ctx) {
		try {
			LinkedHashMap<String, ?> request = ctx.bodyAsClass(LinkedHashMap.class);
			Tranca tranca = criaTranca(request);
			adicionaTranca(tranca);

			ctx.json(tranca);
			ctx.status(200);
		} catch (IllegalArgumentException e) {
			Erro erro = new Erro("422", DADOS_INVALIDOS);
			ctx.json(erro);
	        ctx.status(422);
		}
	}

	public static void obterTranca(Context ctx) {
		try {
			String id = ctx.pathParam(ID_TRANCA);
			Tranca tranca = buscaTranca(id);
			
			ctx.json(tranca);
			ctx.status(200);
		} catch (NullPointerException e) {
			Erro erro = new Erro("404", e.getMessage());
			ctx.json(erro);
	        ctx.status(404);
		}
	}
	
	public static void editarTranca(Context ctx) {
		try {
			String idTranca = ctx.pathParam(ID_TRANCA);
			Tranca trancaAntiga = buscaTranca(idTranca);

			LinkedHashMap<String, ?> request = ctx.bodyAsClass(LinkedHashMap.class);
			Tranca trancaNova = criaTranca(request);

			trancaAntiga.setNumero(trancaNova.getNumero());
			trancaAntiga.setLocalizacao(trancaNova.getLocalizacao());
			trancaAntiga.setAnoDeFabricacao(trancaNova.getAnoDeFabricacao());
			trancaAntiga.setModelo(trancaNova.getModelo());
			trancaAntiga.setStatus(StatusTranca.valueOf(trancaNova.getStatus()));
			
			ctx.json(trancaAntiga);
			ctx.status(200);
		} catch (IllegalArgumentException e) {
			Erro erro = new Erro("422", DADOS_INVALIDOS);
			ctx.json(erro);
	        ctx.status(422);
		}
	}
	
	public static void removerTranca(Context ctx) {
		try {
			String idTranca = ctx.pathParam(ID_TRANCA);
			int idAchado = 0;
			
			for (int i = 0; i < trancas.size(); i++) {
				if (trancas.get(i).getId().equals(idTranca)) {
					trancas.remove(i);
		        	ctx.status(200);
					idAchado = 1;
					break;
				}
			}
			if (idAchado == 0) {
				throw new NullPointerException("idBicicleta não encontrado");
			}
		} catch (NullPointerException e) {
			Erro erro = new Erro("404", e.getMessage());
			ctx.json(erro);
	        ctx.status(404);
		}
	}
	
	public static void bicicletaNaTranca(Context ctx) {
		try {
			String idTranca = ctx.pathParam(ID_TRANCA);
			Tranca tranca = buscaTranca(idTranca);
			Bicicleta bicicleta = ServicosBicicleta.buscaBicicleta(tranca.getBicicleta());
			ctx.json(bicicleta);
			ctx.status(200);
		} catch (IllegalArgumentException e) {
			Erro erro = new Erro("422", "Id da tranca inválido");
			ctx.json(erro);
	        ctx.status(422);
		}
	}
	
	public static void alterarStatus(Context ctx) {
		try {
			String idTranca = ctx.pathParam(ID_TRANCA);
			String acao = ctx.pathParam("acao");
			
			Tranca tranca = buscaTranca(idTranca);
			tranca.setStatus(StatusTranca.valueOf(acao));
			
			ctx.json(tranca);
			ctx.status(200);
		} catch (IllegalArgumentException e) {
			Erro erro = new Erro("422", DADOS_INVALIDOS);
			ctx.json(erro);
	        ctx.status(422);
		}
	}
	
	public static Tranca buscaTranca(String idTranca) {
		for (Tranca tranca : trancas) {
			if (tranca.getId().equals(idTranca)) {
				return tranca;
			}
		}
		throw new NullPointerException("idTranca não encontrado");
	}
	
	public static ArrayList<Tranca> trancasNaLocalizacao (String localizacao) {
		ArrayList<Tranca> trancasNaLocalizacao = new ArrayList<>();

		for (Tranca tranca : trancas) {
			if (tranca.getLocalizacao().equals(localizacao)) {
				trancasNaLocalizacao.add(tranca);
			}
		} 
		
		if (trancasNaLocalizacao.isEmpty()) {
			throw new NullPointerException("Não encontrado");
		}
		
		return trancasNaLocalizacao;
	}

	public static TotemTranca criaTotemTranca(LinkedHashMap<String, ?> request) {
		String idTotem = request.get("idTotem").toString();
		String idTranca = request.get(ID_TRANCA).toString();

		return new TotemTranca(idTotem, idTranca);
	}
	public static Tranca criaTranca(LinkedHashMap<String, ?> request) {
		int numero = Integer.parseInt(request.get("numero").toString());
		String localizacao = request.get("localizacao").toString();
		String anoDeFabricacao = request.get("anoDeFabricacao").toString();
		String modelo = request.get("modelo").toString();
		StatusTranca status = StatusTranca.valueOf(request.get("status").toString());

		return new Tranca(numero, localizacao, anoDeFabricacao, modelo, status);
	}
	public static void adicionaTranca(Tranca tranca) {
		trancas.add(tranca);
	}
}
