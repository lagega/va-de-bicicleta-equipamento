package servicos;

import io.javalin.http.Context;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import equipamento.Bicicleta;
import equipamento.Totem;
import equipamento.Tranca;
import models.Erro;

public class ServicosTotem {
	static final ArrayList<Totem> totens = new ArrayList<>();
	static final String DADOS_INVALIDOS = "Dados Inválidos";
	static final String ID_TOTEM = "idTotem";

	private ServicosTotem() {
		throw new IllegalStateException("Classe de Servicos");
	}
	
	public static void recuperarTotens(Context ctx) {
		ctx.json(totens);
		ctx.status(200);
	}
	
	public static void cadastrarTotem(Context ctx) {
		try {
			LinkedHashMap<String, String> request = ctx.bodyAsClass(LinkedHashMap.class);
			Totem totem = criaTotem(request);
			adicionaTotem(totem);

			ctx.json(totem);
			ctx.status(200);
		} catch (IllegalArgumentException e) {
			Erro erro = new Erro("422", DADOS_INVALIDOS);
			ctx.json(erro);
	        ctx.status(422);
		}
	}
	
	public static void editarTotem(Context ctx) {
		try {
			String idTotem = ctx.pathParam(ID_TOTEM);
			LinkedHashMap<String, String> request = ctx.bodyAsClass(LinkedHashMap.class);
			Totem totemNovo = criaTotem(request);
			Totem totemAntigo = buscaTotem(idTotem);

			totemAntigo.setLocalizacao(totemNovo.getLocalizacao());
			
			ctx.json(totemAntigo);
			ctx.status(200);
		} catch (IllegalArgumentException e) {
			Erro erro = new Erro("422", DADOS_INVALIDOS);
			ctx.json(erro);
	        ctx.status(422);
		}
	}
	
	public static void removerTotem(Context ctx) {
		try {
			String idTotem = ctx.pathParam(ID_TOTEM);
			int idAchado = 0;
			
			for (int i = 0; i < totens.size(); i++) {
				if (totens.get(i).getId().equals(idTotem)) {
					totens.remove(i);
		        	ctx.status(200);
		        	idAchado = 1;
					break;
				}
			}

			if (idAchado == 0) {
				throw new NullPointerException("idTotem não encontrado");
			}
		} catch (NullPointerException e) {
			Erro erro = new Erro("404", e.getMessage());
			ctx.json(erro);
	        ctx.status(404);
		}
	}
	
	public static void listarTrancas(Context ctx) {
		try {
			String idTotem = ctx.pathParam(ID_TOTEM);
			Totem totem = buscaTotem(idTotem);
			String localizacao = totem.getLocalizacao();
			
			ArrayList<Tranca> listaTrancas = ServicosTranca.trancasNaLocalizacao(localizacao);
			ctx.json(listaTrancas);
			ctx.status(200);
		} catch (IllegalArgumentException e) {
			Erro erro = new Erro("422", DADOS_INVALIDOS);
			ctx.json(erro);
	        ctx.status(404);
		}
	}
	
	public static void listarBicicletas(Context ctx) {
		try {
			String idTotem = ctx.pathParam(ID_TOTEM);
			Totem totem = buscaTotem(idTotem);
			String localizacao = totem.getLocalizacao();
			
			ArrayList<Tranca> listaTrancas = ServicosTranca.trancasNaLocalizacao(localizacao);
			ArrayList<Bicicleta> listaBicicletas = new ArrayList<>();
			
			for (Tranca tranca : listaTrancas) {
				String idBicicleta = tranca.getBicicleta();
				if (idBicicleta != "") {
					Bicicleta bicicleta = ServicosBicicleta.buscaBicicleta(idBicicleta);
					listaBicicletas.add(bicicleta);
				}
			}

			ctx.json(listaBicicletas);
			ctx.status(200);
		} catch (IllegalArgumentException e) {
			Erro erro = new Erro("422", DADOS_INVALIDOS);
			ctx.json(erro);
	        ctx.status(404);
		}
	}
	
	public static Totem buscaTotem(String idTotem) {
		for (Totem totem : totens) {
			if (totem.getId().equals(idTotem)) {
				return totem;
			}
		}
		throw new NullPointerException("idTotem não encontrado");
	}

	public static Totem criaTotem(LinkedHashMap<String, String> request) {
		String localizacao = request.get("localizacao");
		return new Totem(localizacao);
	}

	public static void adicionaTotem(Totem totem) {
		totens.add(totem);
	}
}