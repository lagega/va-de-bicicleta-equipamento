package equipamento;

import models.StatusBicicleta;

import java.time.Year;
import java.util.UUID;

public class Bicicleta {
	private String marca;
	private String modelo;
	private String ano;
	private int numero;
	private String status;
	final String id;
	
	
	public Bicicleta(String marca, String modelo, Year ano, int numero, StatusBicicleta status) {
		this.numero = numero;
		this.ano = String.valueOf(ano);
		this.id = UUID.randomUUID().toString();
		this.marca = marca;
		this.modelo = modelo;
		this.status = status.toString();
	}
	
	public int getNumero() {
		return numero;
	}
	
	public void setNumero(int numero) {
		this.numero = numero;
	}
	
	public String getAno() {
		return ano;
	}
	
	public void setAno(String ano) {
		this.ano = String.valueOf(ano);
	}

	public String getId() {
		return id;
	}

	public String getMarca() {
		return marca;
	}
	
	public void setMarca(String marca) {
		this.marca = marca;
	}
	
	public String getModelo() {
		return modelo;
	}
	
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	
	public String getStatus() {
		return status;
	}
	
	public void setStatus(StatusBicicleta status) {
		this.status = status.toString();
	}
}