package equipamento;

import java.util.UUID;

import models.StatusTranca; 

public class Tranca {
	final String id;
	private String bicicleta;
	private int numero;
	private String localizacao;
	private String anoDeFabricacao;
	private String modelo;
	private String status;
	
	public Tranca(int numero, String localizacao, String anoDeFabricacao, String modelo, StatusTranca status) {		
		this.id = UUID.randomUUID().toString();
		this.bicicleta = "";
		this.numero = numero;
		this.localizacao = localizacao;
		this.anoDeFabricacao = anoDeFabricacao;
		this.modelo = modelo;
		this.status = status.toString();
	}

	public String getId() {
		return id;
	}
	public String getBicicleta() {
		return bicicleta;
	}

	public void setBicicleta(String bicicleta) {
		this.bicicleta = bicicleta;
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		if (numero > 0) {
			this.numero = numero;
		}
	}

	public String getLocalizacao() {
		return localizacao;
	}

	public void setLocalizacao(String localizacao) {
		this.localizacao = localizacao;
	}

	public String getAnoDeFabricacao() {
		return anoDeFabricacao;
	}

	public void setAnoDeFabricacao(String anoDeFabricacao) {
		this.anoDeFabricacao = anoDeFabricacao;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(StatusTranca status) {
		this.status = status.toString();
	}
	
}