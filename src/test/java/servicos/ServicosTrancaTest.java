package servicos;

import equipamento.Bicicleta;
import equipamento.Totem;
import equipamento.Tranca;
import kong.unirest.HttpResponse;
import kong.unirest.Unirest;
import models.StatusTranca;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import util.JavalinApp;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

public class ServicosTrancaTest {
    static final JavalinApp app = new JavalinApp();
    static final String PARAMS = "\"numero\":1,\"localizacao\":\"rua x\",\"anoDeFabricacao\":\"2000\",\"modelo\":\"modelo\",\"status\":\"LIVRE\"";
    static Tranca criaTrancaPadrao() {
        Tranca tranca = new Tranca(1, "rua x", "2000", "modelo", StatusTranca.LIVRE);
        ServicosTranca.adicionaTranca(tranca);
        return tranca;
    }

    @BeforeAll
    static void init() {
        app.start(7010);
    }

    @AfterAll
    static void afterAll(){
        app.stop();
    }

    @Test
    void integrarNaRedeSucessoTest() {
        Tranca tranca = new Tranca(1, "rua x", "2000", "modelo", StatusTranca.NOVA);
        ServicosTranca.adicionaTranca(tranca);
        Totem totem = new Totem("rua x");
        ServicosTotem.adicionaTotem(totem);

        ServicosEmail mockEmail = mock(ServicosEmail.class);

        String body = String.format(
                "{" +
                        "  \"idTranca\": \"%s\",\n" +
                        "  \"idTotem\": \"%s\"\n" +
                        "}",
                tranca.getId(),
                totem.getId());

        HttpResponse<String> response = Unirest.post("http://localhost:7010/tranca/integrarNaRede").body(body).asString();
        verify(mockEmail, times(1)).enviaEmail("tranca", "1", "inserida");
        assertEquals(200, response.getStatus());
        assertEquals(StatusTranca.LIVRE.toString(), tranca.getStatus());
    }

    @Test
    void retirarDaRedeSucessoTest() {
        Tranca tranca = criaTrancaPadrao();

        String body = String.format(
                "{ \"idTranca\": \"%s\", \"idTotem\": \"idTotem\" }",
                tranca.getId());

        HttpResponse<String> response = Unirest.post("http://localhost:7010/tranca/retirarDaRede").body(body).asString();
        assertEquals(200, response.getStatus());
        assertEquals(StatusTranca.EM_REPARO.toString(), tranca.getStatus());
    }

    @Test
    void recuperarTrancasSucessoTest() {
        criaTrancaPadrao();

        HttpResponse<String> response = Unirest.get("http://localhost:7010/tranca").asString();
        assertEquals(200, response.getStatus());
        assertTrue(response.getBody().contains(PARAMS));
    }

    @Test
    void cadastrarTrancaSucessoTest() {
        HttpResponse<String> response = Unirest.post("http://localhost:7010/tranca")
                .body(String.format("{ %s }", PARAMS)).asString();

        assertEquals(200, response.getStatus());
        assertTrue(response.getBody().contains(PARAMS));
    }

    @Test
    void obterTrancaSucessoTest() {
        Tranca tranca = criaTrancaPadrao();
        String url = String.format("http://localhost:7010/tranca/%s", tranca.getId());

        HttpResponse<String> response = Unirest.get(url).asString();

        assertEquals(200, response.getStatus());
        assertTrue(response.getBody().contains(PARAMS));
    }

    @Test
    void editarTrancaSucessoTest() {
        Tranca tranca = criaTrancaPadrao();
        String url = String.format("http://localhost:7010/tranca/%s", tranca.getId());
        String novosParams = "\"numero\":1,\"localizacao\":\"rua x\",\"anoDeFabricacao\":\"3000\",\"modelo\":\"modelo\",\"status\":\"OCUPADA\"";

        HttpResponse<String> response = Unirest.put(url).body(String.format("{ %s }", novosParams)).asString();

        assertEquals(200, response.getStatus());
        assertTrue(response.getBody().contains(novosParams));
    }

    @Test
    void removerTrancaSucessoTest() {
        Tranca tranca = criaTrancaPadrao();
        String url = String.format("http://localhost:7010/tranca/%s", tranca.getId());

        HttpResponse<String> response = Unirest.delete(url).asString();
        assertEquals(200, response.getStatus());
        for (Tranca i: ServicosTranca.trancas) {
            if (i.getId().equals(tranca.getId())) {
                throw new AssertionError();
            }
        }
    }

    @Test
    void bicicletaNaTrancaSucessoTest() {
        Tranca tranca = criaTrancaPadrao();
        Bicicleta bicicleta = ServicosBicicletaTest.criaBicicletaPadrao();
        tranca.setBicicleta(bicicleta.getId());

        String url = String.format("http://localhost:7010/tranca/%s/bicicleta", tranca.getId());
        HttpResponse<String> response = Unirest.get(url).asString();

        assertEquals(200, response.getStatus());
        assertTrue(response.getBody().contains("\"marca\":\"marca\",\"modelo\":\"modelo\",\"ano\":\"2000\",\"numero\":1,\"status\":\"DISPONIVEL\""));
    }

    @Test
    void alterarStatusSucessoTest() {
        Tranca tranca = criaTrancaPadrao();
        String url = String.format("http://localhost:7010/tranca/%s/status/OCUPADA", tranca.getId());
        HttpResponse<String> response = Unirest.post(url).asString();
        assertEquals(200, response.getStatus());
        assertEquals("OCUPADA", tranca.getStatus());
    }

    @Test
    void buscaTrancaSucessoTest() {
        Tranca tranca = criaTrancaPadrao();
        Tranca busca = ServicosTranca.buscaTranca(tranca.getId());

        assertEquals(tranca, busca);
    }
    @Test
    void adicionaTrancaTest() {
        Tranca tranca = new Tranca(1, "rua x", "2000", "modelo", StatusTranca.LIVRE);
        ServicosTranca.adicionaTranca(tranca);

        assertTrue(ServicosTranca.trancas.contains(tranca));
    }
}
