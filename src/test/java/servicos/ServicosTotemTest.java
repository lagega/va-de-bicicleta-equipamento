package servicos;

import equipamento.Bicicleta;
import equipamento.Totem;
import equipamento.Tranca;
import kong.unirest.HttpResponse;
import kong.unirest.Unirest;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import util.JavalinApp;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ServicosTotemTest {
    static final JavalinApp app = new JavalinApp();
    static final String localBase = "\"localizacao\":\"rua x\"";
    static Totem criaTotemPadrao() {
        Totem totem = new Totem("rua x");
        ServicosTotem.adicionaTotem(totem);
        return totem;
    }

    @BeforeAll
    static void init() {
        app.start(7010);
    }

    @AfterAll
    static void afterAll(){
        app.stop();
    }

    @Test
    void recuperarTotensTest() {
        criaTotemPadrao();

        HttpResponse<String> response = Unirest.get("http://localhost:7010/totem").asString();
        assertEquals(200, response.getStatus());
        assertTrue(response.getBody().contains(localBase));
    }

    @Test
    void cadastrarTotemTest() {
        HttpResponse<String> response = Unirest.post("http://localhost:7010/totem")
                .body(String.format("{ %s }", localBase)).asString();

        assertEquals(200, response.getStatus());
        assertTrue(response.getBody().contains(localBase));
    }

    @Test
    void removerTotemTest() {
        Totem totem = criaTotemPadrao();
        String url = String.format("http://localhost:7010/totem/%s", totem.getId());

        HttpResponse<String> response = Unirest.delete(url).asString();
        assertEquals(200, response.getStatus());
        for (Totem i: ServicosTotem.totens) {
            if (i.getId().equals(totem.getId())) {
                throw new AssertionError();
            }
        }
    }

    @Test
    void listarTrancasTest() {
        Totem totem = criaTotemPadrao();
        String url = String.format("http://localhost:7010/totem/%s/trancas", totem.getId());
        ServicosTrancaTest.criaTrancaPadrao();
        String padraoTranca = "\"bicicleta\":\"\",\"numero\":1,\"localizacao\":\"rua x\",\"anoDeFabricacao\":\"2000\",\"modelo\":\"modelo\",\"status\":\"LIVRE\"";

        HttpResponse<String> response = Unirest.get(url).asString();
        assertEquals(200, response.getStatus());
        assertTrue(response.getBody().contains(padraoTranca));
    }

    @Test
    void listarBicicletasTest() {
        Totem totem = criaTotemPadrao();
        String url = String.format("http://localhost:7010/totem/%s/bicicletas", totem.getId());

        Tranca tranca = ServicosTrancaTest.criaTrancaPadrao();
        Bicicleta bicicleta = ServicosBicicletaTest.criaBicicletaPadrao();

        tranca.setBicicleta(bicicleta.getId());
        String padraoBicicleta = "\"marca\":\"marca\",\"modelo\":\"modelo\",\"ano\":\"2000\",\"numero\":1,\"status\":\"DISPONIVEL\"";

        HttpResponse<String> response = Unirest.get(url).asString();
        assertEquals(200, response.getStatus());
        assertTrue(response.getBody().contains(padraoBicicleta));
    }

    @Test
    void buscaTotemTest() {
        Totem totem = criaTotemPadrao();
        Totem busca = ServicosTotem.buscaTotem(totem.getId());

        assertEquals(totem, busca);
    }

    @Test
    void adicionaTotemTest() {
        Totem totem = new Totem("rua x");
        ServicosTotem.adicionaTotem(totem);

        assertTrue(ServicosTotem.totens.contains(totem));
    }
}
