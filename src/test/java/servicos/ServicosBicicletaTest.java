package servicos;

import static org.junit.jupiter.api.Assertions.*;

import equipamento.Bicicleta;
import equipamento.Tranca;
import models.StatusBicicleta;
import models.StatusTranca;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import util.JavalinApp;
import kong.unirest.HttpResponse;
import kong.unirest.Unirest;

import java.time.Year;


class ServicosBicicletaTest {

    static final JavalinApp app = new JavalinApp();
    static Bicicleta criaBicicletaPadrao() {
        Bicicleta bicicleta = new Bicicleta("marca", "modelo", Year.of(2000), 1, StatusBicicleta.DISPONIVEL);
        ServicosBicicleta.adicionaBicicleta(bicicleta);
        return bicicleta;
    }

    @BeforeAll
    static void init() {
        app.start(7010);
    }

    @AfterAll
    static void afterAll(){
        app.stop();
    }

    @Test
    void recuperarBicicletasPreenchidoTest() {
        criaBicicletaPadrao();

        HttpResponse<String> response = Unirest.get("http://localhost:7010/bicicleta").asString();
        String resultadoEsperado = "[{\"marca\":\"marca\",\"modelo\":\"modelo\",\"ano\":\"2000\",\"numero\":1,\"status\":\"DISPONIVEL\"";
        assertEquals(200, response.getStatus());
        assertTrue(response.getBody().contains(resultadoEsperado));
    }

    @Test
    void cadastrarBicicletaSucessoTest() {
        HttpResponse<String> response = Unirest.post("http://localhost:7010/bicicleta")
                .body("{" +
                        "  \"marca\": \"marca\",\n" +
                        "  \"modelo\": \"modelo\",\n" +
                        "  \"ano\": \"2000\",\n" +
                        "  \"numero\": 1,\n" +
                        "  \"status\": \"DISPONIVEL\"\n" +
                        "}"
                ).asString();

        String resultadoEsperado = "{\"marca\":\"marca\",\"modelo\":\"modelo\",\"ano\":\"2000\",\"numero\":1,\"status\":\"DISPONIVEL\"";
        assertEquals(200, response.getStatus());
        assertTrue(response.getBody().contains(resultadoEsperado));
    }

    @Test
    void cadastrarBicicletaErroTest() {
        HttpResponse<String> response = Unirest.post("http://localhost:7010/bicicleta").body(
                "{" +
                        "  \"marca\": \"marca\",\n" +
                        "  \"modelo\": \"modelo\",\n" +
                        "  \"ano\": \"lala\",\n" +
                        "  \"numero\": 1\n" +
                        "}"
        ).asString();
        assertEquals(422, response.getStatus());
    }

    @Test
    void integrarNaRedeSucessoTest() {
        Bicicleta bicicleta = new Bicicleta("marca", "modelo", Year.of(2000), 1, StatusBicicleta.NOVA);
        ServicosBicicleta.adicionaBicicleta(bicicleta);
        Tranca tranca = new Tranca(1, "rua x", "1990", "modelo", StatusTranca.LIVRE);
        ServicosTranca.adicionaTranca(tranca);

        String body = String.format(
                "{" +
                        "  \"idTranca\": \"%s\",\n" +
                        "  \"idBicicleta\": \"%s\"\n" +
                        "}",
                tranca.getId(),
                bicicleta.getId());

        HttpResponse<String> response = Unirest.post("http://localhost:7010/bicicleta/integrarNaRede").body(body).asString();
        assertEquals(200, response.getStatus());
        assertEquals(StatusBicicleta.DISPONIVEL.toString(), bicicleta.getStatus());
        assertEquals(StatusTranca.OCUPADA.toString(), tranca.getStatus());
    }

    @Test
    void integrarNaRedeErroBicicletaTest() {
        Bicicleta bicicleta = criaBicicletaPadrao();

        String body = String.format(
                "{" +
                        "  \"idTranca\": \"lala\",\n" +
                        "  \"idBicicleta\": \"%s\"\n" +
                        "}",
                bicicleta.getId());

        HttpResponse<String> response = Unirest.post("http://localhost:7010/bicicleta/integrarNaRede").body(body).asString();

        assertEquals(422, response.getStatus());
    }

    @Test
    void integrarNaRedeErroTotemTest() {
        Bicicleta bicicleta = new Bicicleta("marca", "modelo", Year.of(2000), 1, StatusBicicleta.NOVA);
        ServicosBicicleta.adicionaBicicleta(bicicleta);
        Tranca tranca = new Tranca(1, "rua x", "1990", "modelo", StatusTranca.OCUPADA);
        ServicosTranca.adicionaTranca(tranca);

        String body = String.format(
                "{" +
                        "  \"idTranca\": \"%s\",\n" +
                        "  \"idBicicleta\": \"%s\"\n" +
                        "}",
                tranca.getId(),
                bicicleta.getId());

        HttpResponse<String> response = Unirest.post("http://localhost:7010/bicicleta/integrarNaRede").body(body).asString();

        assertEquals(422, response.getStatus());
    }

    @Test
    void retirarDaRedeSucessoTest() {
        Bicicleta bicicleta = criaBicicletaPadrao();
        bicicleta.setStatus(StatusBicicleta.REPARO_SOLICITADO);
        Tranca tranca = new Tranca(1, "rua x", "1990", "modelo", StatusTranca.OCUPADA);
        tranca.setBicicleta(bicicleta.getId());
        ServicosTranca.adicionaTranca(tranca);

        String body = String.format(
                "{" +
                        "  \"idTranca\": \"%s\",\n" +
                        "  \"idBicicleta\": \"%s\"\n" +
                        "}",
                tranca.getId(),
                bicicleta.getId());

        HttpResponse<String> response = Unirest.post("http://localhost:7010/bicicleta/retirarDaRede").body(body).asString();
        assertEquals(200, response.getStatus());
        assertEquals(StatusBicicleta.EM_REPARO.toString(), bicicleta.getStatus());
        assertEquals(StatusTranca.LIVRE.toString(), tranca.getStatus());
    }

    @Test
    void obterBicicletaSucessoTest() {
        Bicicleta bicicleta = criaBicicletaPadrao();
        String url = String.format("http://localhost:7010/bicicleta/%s", bicicleta.getId());

        HttpResponse<String> response = Unirest.get(url).asString();
        String resultadoEsperado = "{\"marca\":\"marca\",\"modelo\":\"modelo\",\"ano\":\"2000\",\"numero\":1,\"status\":\"DISPONIVEL\"";

        assertEquals(200, response.getStatus());
        assert(response.getBody().contains(resultadoEsperado));
    }

    @Test
    void editarBicicletaSucessoTest() {
        Bicicleta bicicleta = criaBicicletaPadrao();
        String url = String.format("http://localhost:7010/bicicleta/%s", bicicleta.getId());
        String body = "{\"marca\":\"marca\",\"modelo\":\"modelo\",\"ano\":\"3000\",\"numero\":1,\"status\":\"EM_REPARO\"";

        HttpResponse<String> response = Unirest.put(url).body(String.format("%s }", body)).asString();

        assertEquals(200, response.getStatus());
        assert(response.getBody().contains(body));
    }

    @Test
    void removerBicicletaSucessoTest() {
        Bicicleta bicicleta = criaBicicletaPadrao();
        String url = String.format("http://localhost:7010/bicicleta/%s", bicicleta.getId());

        HttpResponse<String> response = Unirest.delete(url).asString();
        assertEquals(200, response.getStatus());
        for (Bicicleta i: ServicosBicicleta.bicicletas) {
            if (i.getId().equals(bicicleta.getId())) {
                throw new AssertionError();
            }
        }
    }

    @Test
    void alterarStatusSucessoTest() {
        Bicicleta bicicleta = criaBicicletaPadrao();
        String url = String.format("http://localhost:7010/bicicleta/%s/status/NOVA", bicicleta.getId());
        HttpResponse<String> response = Unirest.post(url).asString();
        assertEquals(200, response.getStatus());
        assertEquals("NOVA", bicicleta.getStatus());
    }

    @Test
    void buscaBicicletaSucessoTest() {
        Bicicleta bicicleta = criaBicicletaPadrao();
        Bicicleta busca = ServicosBicicleta.buscaBicicleta(bicicleta.getId());

        assertEquals(bicicleta, busca);
    }
    @Test
    void adicionaBicicletaTest() {
        Bicicleta bicicleta = new Bicicleta("marca", "modelo", Year.of(2000), 1, StatusBicicleta.DISPONIVEL);
        ServicosBicicleta.adicionaBicicleta(bicicleta);

        assertTrue(ServicosBicicleta.bicicletas.contains(bicicleta));
    }
}
